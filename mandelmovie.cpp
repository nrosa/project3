#include "bitmap.h"
#include "mandel.h"
#include <string.h>
#include <string>
#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <set>

using namespace std;
void usage();


int main(int argc, char *argv[]) {
	
	//PARSE COMMAND LINE
	int processnum = 0;// initiallizing processnum
	set<int> PIDS;
	if(argc == 1){
		cout<<"Default value set for process num"<<endl;
		processnum = 1;// whatever default value
	}	

	if (argc > 2){
		cout<<"Incorrect number of arguments. Try again with just one argument."<<endl;
		usage(); // function to be written 
		exit(EXIT_FAILURE);
	}

	if (argc == 2 ){
		processnum = stoi(argv[1]);		
	}
	
	//INITIALIZATIONS
	int count = 0;
	int full = 0;
	double a=2;
	double s= a;
	

	//COUNT UP TO MAX PROCESSES (50)
	while (count < 50){
	
		//CLEAR OUT THE SET IF A PROCESS HAS FINISHED
		for (set<int>::iterator it= PIDS.begin(); it!= PIDS.end(); it++){
			int status = 0;
			int r = waitpid(*it, &status, WNOHANG);												//used to check if the process has finished
			if(r == -1){
				cout<<"There was an error checking if the process was still running"<<endl;
			}
			else if (r != 0){
				PIDS.erase(it);																	//remove any finished processes from the set
				full-=1;																		//whenever a process finishes need to subtract one from the full
			}
		}


		//IF WE CAN RUN MORE PROCESSES AT ONCE
		if (full < processnum){

			//SET UP THE NULL TERMINATED ARRAY
			char * arguments[100];
			//string command = "mandel -x 0.2869325 -y 0.0142905 -s 0.000001 -W 1024 -H 1024 -m 1000 -n 8 -o /tmp/mandel" + to_string(count); 		//command for combination test
			string command = "mandel -o /tmp/mandel" + to_string(count) + ".bmp -x .286932 -y 0.014287 -m 1000 -W 1000 -H 1000 -s " + to_string(s);// command we are running 
			char * newCommand = new char [command.length()+1];
			strcpy (newCommand, command.c_str());

			s -= 0.04081622;																	//update s value for each of the generated files
	
			char *pch = strtok(newCommand, " ");
			int i=0;
			arguments[i] = pch;
			i++;
			while((pch = strtok(NULL, " "))){
				arguments[i] = pch;
				i++;
			}
			arguments[i] = NULL;																//null terminate the array


			//START THE FORKING
			int rc = fork();
			count +=1;																			//every time fork happens count and full are increased
			full+=1;
			if (rc<0){							//fork failed condition
				fprintf(stderr,"fork attempt failed\n");
				exit(EXIT_FAILURE);
			}
			else if (rc==0){					//child process created
				execvp(arguments[0], arguments);
			}
			else{								//parent
				PIDS.insert(rc);																//when a process has been successfully forked add the PID to the set
			} 


		} //end of if full
	} //end of while
		for (set<int>::iterator it= PIDS.begin(); it!= PIDS.end(); it++){						//wait for each of the remaining processes after the count has reached the limit 
			int status = 0;																		//need to wait for every process to complete
			int r = waitpid(*it, &status, 0);
			if(r == -1){
				cout<<"There was an error checking if the process was still running"<<endl;
			}
			else if (r != 0){
				PIDS.erase(it);																	//empty the set
			}
		}

	return 0;
}



void usage(){
	cout<<"The command format for using mandelmovie is as follows:"<<endl;
	cout<<"mandelmovie processnum"<<endl;
}
