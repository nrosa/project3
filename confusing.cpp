#include "bitmap.h"
#include "mandel.h"
#include <string.h>
#include <string>
#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <set>

using namespace std;
void usage();


int main(int argc, char *argv[]) {
	// Not implemented
	int processnum = 0;// initiallizing processnum
	set<int> PIDS;
	if(argc == 1){
		cout<<"Default value set for process num"<<endl;
		processnum = 1;// whatever default value
	}	

	if (argc > 2){
		cout<<"Incorrect number of arguments. Try again with just one argument."<<endl;
		usage(); // function to be written 
		exit(EXIT_FAILURE);
	}

	if (argc == 2 ){
		processnum = stoi(argv[1]);		
	}
	
	cout<<processnum<<endl;
	// Running fork process
	//needs to be in a loop create until it is equal to process num
	int count = 0;
	int full = 0;
	char * arguments[100];

	//string command = "date";// command we are running 
	//char * newCommand = new char [command.length()+1];
	//strcpy (newCommand, command.c_str());
	arguments[0] = "date";
	arguments[1] = NULL;
	//string scount = to_string(count);
	//char * ccount = new char [scount.length()+1];
	//strcpy(ccount,scount.c_str());
	double a=2;
	double b=0.000005;
	while (count < 10){
		//while(full < processnum){
		/*
			a *= exp(log(b/a)/(50-1));
			
			string command = "mandel";// command we are running 
			char * newCommand = new char [command.length()+1];
			strcpy (newCommand, command.c_str());
	
			string arg0 = "-o mandel" + to_string(count) + ".bmp";
			char * newarg0 = new char [arg0.length()+1];
			strcpy (newarg0, arg0.c_str());

			string filter1 = "-x .286932 -y 0.014287";//stays constant
			char *xy  = new char [filter1.length()+1];
			strcpy (xy, filter1.c_str());

			string filter2 = "-m 1000 -W 1000 -H 1000";// stays constant
			char *yz = new char [filter2.length()+1];
			strcpy(yz, filter2.c_str());

			string s = "-s";// stays constant
			char *lo = new char [s.length()+1];
			strcpy(lo, s.c_str());
			char *so;
			if (count == 0){
				string s1 = "2";
				so = new char [s1.length()+1];
				strcpy(so, s1.c_str() );
			}
			else{
				string s1 = to_string(a);
				so = new char [s1.length()+1];
				strcpy(so, s1.c_str() );
			}
	

			char *arguments[100];// array to carry the arguments
			arguments[0] = newCommand;
			arguments[1] = xy;// unchanging values of x and y 
			arguments[2] = lo;
			arguments[3] = so; // initial value of s 
			arguments[4] = yz;
			arguments[5] = newarg0;
			arguments[6] = NULL;

			cout<<"Before forking"<<endl;

			*/
		for (set<int>::iterator it= PIDS.begin(); it!= PIDS.end(); it++){
			int status = 0;
			int r = waitpid(*it, &status, WNOHANG);
			if(r == -1){
				cout<<"There was an error checking if the process was still running"<<endl;
			}
			else if (r != 0){
				PIDS.erase(it);
				full-=1;
			}
		}

		if (full < processnum){
			int rc = fork();
			count +=1;
			full+=1;
			if (rc<0){//fork failed condition
				fprintf(stderr,"fork attempt failed\n");
				exit(EXIT_FAILURE);
			}
			else if (rc==0){//child process created
				execvp(arguments[0], arguments);
			}
			else{//parent
				cout<<"In parent"<<endl;
				PIDS.insert(rc);
			} 


		} //end of if full
	} //end of while
cout<<count<<endl;
cout<<"Out of while"<<endl;	
return 0;
}

void usage(){
	cout<<"The command format for using mandelmovie is as follows:"<<endl;
	cout<<"mandelmovie processnum"<<endl;
}
